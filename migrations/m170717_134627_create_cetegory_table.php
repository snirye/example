<?php

use yii\db\Migration;

/**
 * Handles the creation of table `cetegory`.
 */
class m170717_134627_create_cetegory_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('cetegory', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('cetegory');
    }
}
