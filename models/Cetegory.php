<?php

namespace app\models;
use yii\helpers\ArrayHelper;

use Yii;


/**
 * This is the model class for table "cetegory".
 *
 * @property integer $id
 * @property string $name
 */
class Cetegory extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'cetegory';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
        ];
    }
	
	// this 'getCategories' weas added to make a dropDown list in the forms
	public static function getCategories()
	{
		$allcategories = self::find()->all();
		$allcategoriesArray = ArrayHelper::
					map($allcategories, 'id', 'name');
		return $allcategoriesArray;						
	}
}
