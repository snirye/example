<?php
namespace app\rbac;

use yii\rbac\Rule;
use Yii; 

class OwnActivityRule extends Rule
{

	public $name = 'OwnActivityRule';

	public function execute($user, $item, $params)
	{
		if (!Yii::$app->user->isGuest) {
			return isset($params['category']) ? $params['category']->id == $user : false;
			}
		return false;
	}
}