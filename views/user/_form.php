<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\User;
use app\models\Cetegory;

/* @var $this yii\web\View */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-form">

    <?php $form = ActiveForm::begin(); ?>

    <!--?= $form->field($model, 'id')->textInput() ?-->

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'username')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'password')->passwordInput(['maxlength' => true]) ?>
	
	<!--?= $form->field($model, 'authkey')->textInput(['maxlength' => true]) ?-->
	
	

	
	<?php if (!$model->isNewRecord) { ?>
		<?= $form->field($model, 'role')->dropDownList(User::getRoles()) ?>
	<?php } else { ?>
		<div style="display:none;"><?= $model->isNewRecord ? $form->field($model, 'role')->textInput(['value'=>simpleUser]) : ''?></div>
	<?php } ?>
	
	
	<!--                 ($model,'name of var')           (name of model::name of function) -->
		<?php if (!$model->isNewRecord) { ?>
		<?= $form->field($model, 'categoryid')->dropDownList(Cetegory::getCategories()) ?>
	<?php } else { ?>
		<div style="display:none;"><?= $model->isNewRecord ? $form->field($model, 'categoryid')->textInput(['value'=>simpleUser]) : ''?></div>
	<?php } ?>



    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>